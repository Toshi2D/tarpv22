def length(icode:str):
    # print(len(icode)) # Debug
    if len(icode) == 11:
        flag = True
    else:
        flag = False
    return flag

def gender(icode:str):
    icode_list = list(map(int, icode))
    if icode_list[0] in [1, 3, 5]:
        g = "male"
    elif icode_list[0] in [2, 4, 6]:
        g = "female"
    else: g = "error"
    
    return g

def birthday(icode:str):
    icode_list = list(map(int, icode))
    y=str(icode_list[1])+str(icode_list[2])
    m=str(icode_list[3])+str(icode_list[4])
    d=str(icode_list[5])+str(icode_list[6])
    
    if(int(m)>0 and int(m)<13) and (int(d)>0 and int(d)<32):
        if icode_list[0] in [1,2]:
            yy="18"
        elif icode_list[0] in [3,4]:
            yy="19"
        elif icode_list[0] in [5,6]:
            yy="20"
        bday = d+"."+m+"."+yy+y
    else:
        bday="viga"
    return bday

def birthplace(icode:str)->str:
    icode_list = list(icode)
    tahed_8910 = icode_list[7] + icode_list[8] + icode_list[9]
    t = int(tahed_8910)
    if 1 < t < 10:
        hospital = "Kuresaare Haigla"
    elif 11 < t < 19:
        hospital ="Tartu Ülikooli Naistekliinik, Tartumaa, Tartu"
    elif 21 < t < 220:
        hospital ="Ida-Tallinna Keskhaigla, Pelgulinna sünnitusmaja, Hiiumaa, Keila, Rapla haigla, Loksa haigla"
    elif 221 < t < 270:
        hospital ="Ida-Viru Keskhaigla (Kohtla-Järve, endine Jõhvi)"
    elif 271 < t < 370:
        hospital = "Maarjamõisa Kliinikum (Tartu), Jõgeva Haigla"
    elif 371 < t < 420:
        hospital = "Narva Haigla"
    elif 421 < t < 470: 
        hospital = "Pärnu Haigla"
    elif 471 < t < 490:
        hospital = "Pelgulinna Sünnitusmaja (Tallinn), Haapsalu haigla"
    elif 491 < t < 520:
        hospital = "Järvamaa Haigla (Paide)"
    elif 521 < t < 570:
        hospital = "Rakvere, Tapa haigla"
    elif 571 < t < 600:
        hospital  = "Valga Haigla"
    elif 601 < t < 650:
        hospital = "Viljandi Haigla"
    elif 651 < t < 700:
        hospital = "Lõuna-Eesti Haigla (Võru), Põlva Haigla"
    else:
        hospital = "Ei ole sündinud Eestis"
    return hospital

def lastnum(icode:str)->int:
    list1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 1]
    list2 = [3, 4, 5, 6, 7, 8, 9, 1, 2, 3]
    sum1=0
    icode_list=list(icode)
    icode_list=list(map(int,icode_list))
    for i in range(0,10):
        sum1+=icode_list[i]*list1[i]
    s=(sum1//11)*11
    left=sum1-s
    if 1 <= left <= 9:
        return left
    else:
        sum1=0
        for i in range(0,10,1):
            sum1+icode_list[i]*list2[i]
        s=(sum1//11)*11
        left=sum1-s
        if left != 10:
            return left
        else:
            left = 0
            return left
