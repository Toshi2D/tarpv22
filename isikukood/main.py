import icodemodule
import sys

print(" ! Type exit to quit")
history_correct = []
history_incorrect = []

while True:
    icode = input("\n - Insert Estonian ID code: ")
    
    if icode == "exit":
        sys.exit()

    if icodemodule.length(icode) == False: print(" ! Wrong ID length")

    else:
        g = icodemodule.gender(icode)
        if g == "error":
            print(" ! Incorrect value")
            break
        else:
            pass

    gender = icodemodule.gender(icode)
    birthday = icodemodule.birthday(icode)
    birthplace = icodemodule.birthplace(icode)
    lastnum = icodemodule.lastnum(icode)
    
    icode_list = list(map(int, icode))
    
    if lastnum == icode_list[10]:
        print("This person is " + gender + ". This personl birthday is " + birthday + " . His/she's birtplace is: " + birthplace)
        history_correct.append(icode)
        print(history_correct)
    else:
        print(" ! This code is incorrect")
        history_incorrect.append(icode)
        print(history_incorrect)
