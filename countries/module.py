europe = {
    'Albaania': 'Tirana',
    'Andorra': 'Andorra-la-Vella',
    'Armeenia': 'Jerevan',
    'Aserbaidžaan': 'Bakuu',
    'Austria': 'Viin',
    'Belgia': 'Brüssel',
    'Bosnia-ja-Hertsegoviina': 'Sarajevo',
    'Bulgaaria': 'Sofia',
    'Eesti': 'Tallinn',
    'Gruusia': 'Thbilisi',
    'Hispaania': 'Madrid',
    'Horvaatia': 'Zagreb',
    'Iirimaa': 'Dublin',
    'Island': 'Reykjav?k',
    'Itaalia': 'Rooma',
    'Kasahstan': 'Astana',
    'Kreeka': 'Ateena',
    'Küpros': 'Nikosia',
    'Leedu': 'Vilnius',
    'Liechtenstein': 'Vaduz',
    'Luksemburg': 'Luxembourg',
    'Läti': 'Riia',
    'Madalmaad': 'Amsterdam',
    'Makedoonia': 'Skopje',
    'Malta': 'Valletta',
    'Moldova': 'Chisinau',
    'Monaco': 'Monaco',
    'Montenegro': 'Podgorica',
    'Norra': 'Oslo',
    'Poola': 'Varssavi',
    'Portugal': 'Lissabon',
    'Prantsusmaa': 'Pariis',
    'Rootsi': 'Stockholm',
    'Rumeenia': 'Bukarest',
    'Saksamaa': 'Berliin',
    'San-Marino': 'San-Marino',
    'Serbia': 'Belgrad',
    'Slovakkia': 'Bratislava',
    'Sloveenia': 'Ljubljana',
    'Soome': 'Helsingi',
    'Suurbritannia': 'London',
    'Šveits': 'Bern',
    'Taani': 'Kopenhaagen',
    'Tšehhi': 'Praha',
    'Türgi': 'Ankara',
    'Ukraina': 'Kiiev',
    'Ungari': 'Budapest',
    'Valgevene': 'Minsk',
    'Vatikan': 'Vatican',
    'Venemaa': 'Moskva'
}

import random

def search(userinput):
    capital = europe.get(userinput.capitalize())
    print()
    if capital:
        print(f" ! The capital of {userinput.capitalize()} is {capital}")
    else:
        print(f" ! Sorry, {userinput.capitalize()} is not in the list.")
        new(userinput)

        
def new(userinput):
    new_capital = input(f" | What is the capital of {userinput.capitalize()}? ")
    europe[userinput.capitalize()] = new_capital
    print(f" ! Added {userinput.capitalize()} ({new_capital}) to the list of European capitals.")

def show():
    print(europe)

def edit(userinput):
    capital = europe.get(userinput.capitalize())
    if capital:
        print(f"The capital of {userinput.capitalize()} is {capital}")
        new_capital = input(f"Enter a new capital for {userinput.capitalize()} or press ENTER to keep the current value: ")
        if new_capital:
            europe[userinput.capitalize()] = new_capital
            print(f"Updated the capital of {userinput.capitalize()} to {new_capital}")
        else:
            print(f"Kept the capital of {userinput.capitalize()} as {capital}")
    else:
        print(f"Sorry, {userinput.capitalize()} is not in the list.")

def test():
    questions = list(europe.items())
    random.shuffle(questions)
    num_correct = 0
    num_total = len(questions)
    for country, capital in questions:
        guess = input(f"What is the capital of {country}? ")
        if guess.lower() == capital.lower():
            print("Correct!")
            num_correct += 1
        else:
            print(f"Sorry, the capital of {country} is {capital}.")
    score = num_correct / num_total * 100
    print(f"You got {num_correct} out of {num_total} questions correct ({score:.2f}%).")