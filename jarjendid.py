european_capitals = ['Minsk', 'London', 'Paris', 'Rome', 'Madrid', 'Amsterdam', 'Berlin', 'Warsaw', 'Stockholm', 'Copenhagen']

european_capitals.sort()

print('List of European capitals:')
for i, capital in enumerate(european_capitals):
    print(f"{i+1}. {capital}")

new_capital1 = input('Enter the name of a new capital: ')
new_capital2 = input('Enter the name of another new capital: ')
european_capitals.extend([new_capital1, new_capital2])

european_capitals.sort()

print('Updated list of European capitals:')
for i, capital in enumerate(european_capitals):
    print(f"{i+1}. {capital}")

print(f"There are {len(european_capitals)} European capitals in our list.")
