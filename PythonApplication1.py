﻿import random
## 1

x = int(input("Sise number 0 - 9: "))

for b in range(x):
    print()
    print(" ~~~~~")
    print("|_____|")
    print("| []  |")
    print(" -----")

## 2

grades1 = [1, 2, 3, 4, 3, 3, 3, 5]
grades2 = [2, 3, 4, 2, 5, 3, 2, 1]

x = sum(grades1)
y = sum(grades2)
x = x / 8
y = y /8

print("Esimesel klassil: " + str(x) + " Teisel: " + str(y))

## 3 

minimum = 51
maximum = 0
c = 0
for x in range(0, 20, 1):
    a = random.randint(1, 5)
    if a < minimum:
        minimum = a
    elif a > maximum:
        maximum = a
print(minimum)
print(maximum)

## 4 

n = int(input("Kirjuta mittu inimest piirkonnas on.: "))

districts = 12
population = 0
area = 0

for i in range(districts):
    district_population = random.randint(1000, 10000)
    district_area = random.randint(100, 1000)
    population += district_population
    area += district_area

average_density = population / area
print("Keskmine rahvastikutihedus:", average_density, "inimene/km^2")

## 5

x_min = float(input("Sisestage x minimaalne väärtus: "))
x_max = float(input("Sisestage x maksimaalne väärtus: "))
step = float(input("Juurdekasvu väärtuse x sisestamine: "))

x = x_min
print("x\t\ty")
while x<=x_max:
    y = -0.5*x + x
    print(x,"\t",y)