import math
import random

err = "Incorrect data"

## 1
print()

try:
    c = float(input("Anna umbermoot: "))
    if c>0:
        d = round(c / math.pi, 2)
        print(f"Puu labimoot = {d}")
    else:
        print("C must be bigger that 0")
except:
    print(err)

## 2 
print()

N = float(input("side 1: "))
M = float(input("side 2: "))
if M>0 and N>0:
    d=round(math.sqrt(N**2+M**2))
    print(d)
else:
    print(err)

## 3
print()

aeg = float(input("Mitu tundi kulus soiduks? "))
teepikkus = float(input("Mitu kilomeetrit soitsid? "))
if aeg>0 and teepikkus>0:
    kiirus = teepikkus / aeg
    print("Sinu kiirus oli " + str(kiirus) + " km/h")
else:
    print(err)

## 4
print()

a = int(input("Type 1 num: "))
b = int(input("Type 2 num: "))
c = int(input("Type 3 num: "))
d = int(input("Type 4 num: "))
e = int(input("Type 5 num: "))
if a>0 and b>0 and c>0 and d>0 and e>0:
    r = a+b+c+d+e
    r = r / 5
    print(round(r))
else:
    print(err)


## 5
print()

print('  @..@')
print(' (----)')
print('( \__/ )')
print('^^ "" ^^')

# 6
print()

r = input("(G)enerate or (T)type: ")
if r == "g":
    a = random.randint(1,100)
    b = random.randint(1,100)
    c = random.randint(1,100)
else:
    a = int(input(" 1 side: "))
    b = int(input(" 2 side: "))
    c = int(input(" 3 side: "))

if a>0 and b>0 and c>0:
    p = a+b+c
    print("answ: " + str(p))
else:
    print(err)

# 7
print()

p = random.randint(1,6)
price = (12.9*1.1)/p
print(str(price))

# 8
#print()

l = float(input("kutuse liitride kogus: "))
km = float(input("labitud kilomeetrid: "))
if l>0 and km>0:
    kulu = (l/km)*100
else:
    print(err)

# 9
#print()

m = int(input("min: "))
if m>0:
    m = m/60
    tee = m*29.9
    print(str(tee))
else:
    print(err)

# 10

print()
m = int(input("min: "))
if m>0:
    h = m//60
    m = m%60
    print(f"{h}:{m}")
else:
    print(err)
