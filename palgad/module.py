import sys

def exit():
    sys.exit()

def AddData(p:list, w:list):
    print(" ! Adding data")
    # This function stand for adding new peoples and adding wage for them
    hmp = int(input(" - How much ppl's u wanna add?: "))    
    for x in range(hmp):
        name = str(input(" - Name: "))
        wage = int(input(" - Wage: "))
        p.append(name)
        w.append(wage)
    wsc = int(input(" - Wanna see changes? 1-y 0-n"))
    if wsc == 1:
        print()
        print(p)
        print(w)
    else:
        return p, w
    return p, w

def DeleteData(p:list, w:list):
    # Deleting data from ppls list and wage list
    print(" ! Removing data")
    name = str(input(" - Name:"))
    n = p.count(name)
    if name in p: 
        for x in range(n):
            rmp = p.index(name)
            p.pop(rmp)
            w.pop(rmp)
    else:
        print("! Person not found")
    return p, w
   
def ViewData(p:list, w:list):
    # Show list with all data
    print(p)
    print(w) 

def BiggestWage(p:list, w:list):                    #NOT DONE
    # Show's biggest wage and name of person
    maxw = max(w)
    print(maxw)
    n = w.count(maxw)
    for x in range(n):
        print(x)
        wagemax = w[x]
        #person = p[wagemax]
        #print(" | Biggest wage is: " + str(wagemax) + ". And owner is: " + str(person))

def LowestWage(p:list, w:list):
    # Show's lowest wage and person name
    minw = min(w)
    wagemin = w.index(minw)
    person = p[wagemin]
    print(" | Lowest wage is: " + str(minw) + ". And owner is: " + str(person))

def SameWage(p:list, w:list):
    # Show's same wage and name
    dublicate = [x for x in w if w.count(x)>1]
    dublicate = list(set(dublicate))
    for wage in dublicate:
        n=w.count(wage)
        k =- 1
        for j in range(n):
            k = w.index(wage,k+1)
            name=p[k]
            print(name + " got " + str(wage))
