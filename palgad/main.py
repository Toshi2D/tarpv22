import module
ppls = ["Vasja", "Petja", "Petr","Vanja", "Vasili", "Tolik"]
wage = [1200, 1300, 1300, 1000, 800, 1000]

while True:
    menu = str(input(" - Menu: "))
    if menu == "0" or menu == "exit":
        module.exit()
    elif menu == "1" or menu == "add":
        module.AddData(ppls, wage)
    elif menu == "2" or menu == "remove":
        module.DeleteData(ppls, wage)
    elif menu == "3" or menu == "view":
        module.ViewData(ppls, wage)
    elif menu == "4" or menu == "biggest":
        module.BiggestWage(ppls, wage)
    elif menu == "5" or menu == "lowest":
        module.LowestWage(ppls, wage)
    elif menu == "6" or menu == "same":
        module.SameWage(ppls, wage)
    else: print(" ! Command not found")
