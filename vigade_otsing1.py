from math import *

print("Ruudu karakteristikud")
a=input('Sisesta ruudu kulje pikkus => ')
S=float(a)**2
print("Ruudu pindala", S)
P=4*float(a)
print("Ruudu umbermoot", P)
di=float(a)*sqrt(2)
print("Ruudu diagonaal", round(di,2))
print()
print("Ristkuliku karakteristikud")
b=float(input("Sisesta ristkuliku 1. kulje pikkus => "))
c=float(input("Sisesta ristkuliku 2. kulje pikkus => "))
S=b*c
print('Ristkuliku pindala', S)
P=2*(b+c)
print("Ristkuliku umbermoot", P)
di=float(sqrt(b**2+c**2))
print("Ristkuliku diagonaal", round(di, 1))
print()
print("Ringi karakteristikud")
r=int(input('Sisesta ringi raadiusi pikkus => '))
d=2*r
print("Ringi labimoot", round(d, 1))
S=pi*r**2
print("Ringi pindala", round(S, 2))
C=2*pi*r
print("Ringjoone pikkus", round(C, 2))
