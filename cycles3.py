import random
import string

########################## 0

print("Guess number from 0 to 50")

number = random.randrange(1, 50)

while True:
    answ = int(input("Your guess: "))
    if number == answ:
        print("Good job!")
        break
    else: print("Try again!")

## 0 - 1

for _ in iter(int, 1):
    answ = int(input("Your guess: "))
    if number == answ:
        print("Good job!")
        break
    else: print("Try again!")

########################## 0 - 2

print("Arva taht - (Aa kuni Zz)")
letter = random.choice(string.ascii_letters)

while True:
    answ = str(input("Teie oletus: "))
    if letter == answ:
        print("Tubli!")
        break
    else: print("Provi uuesti!")

## 0, 2 - 2

for _ in iter(int, 1):
    answ = str(input("Your guess: "))
    if letter == answ:
        print("Good job!")
        break
    else: print("Try again!")

########################## 22

n = 1

while True:
    print("Tahan kommi!")
    a = str(input())
    if a.lower() == "komm" :
        print("Aitah! Oli vaja " + str(n) + " katse.")
        break
    else:
        n = n + 1

## 22 - 2

n = 1

for _ in iter(int, 1):
    print("tahan kommi!")
    a = str(input())
    if a == "komm":
        print("Aitah! Oli vaja " + str(n) + " katse.")
        break
    else:
        n = n + 1
        print("Tahan kommi!")

########################## 10

x = 1
while True:
    if x > 100:
        break
    elif x % 5 == 0:
        print(x, end =" ")
    x += 1

## 10 - 2

for x in range(1, 101, 1):
    if x % 5 == 0:
        print(x, end =" ")



########################## 16

ans = random.randint(1, 10)
while True:
    g = input("Guess number(1, 10)")
    if g.isdigit() == False:
        print("Vale andmetuup!")
    g = int(g)
    if g > 10 or g < 1:
        print("Vahemik on vale!")
    if g == ans:
        print("Oige! Sa arvasid ara!")
        break
    else: print("Vale provi uuesti!")

########################## 13

print("arv ", "   ruut", "   kuup")
for i in range(1, 11):
    print(f" {i}   {i ** 2}   {i ** 3}")

########################## 8

while True:
    try:
        mainnumber = int(input("Vali number 1-100: "))
        break
    except ValueError:
        print("See pole tais arv")
if mainnumber < 1 or mainnumber > 100:
    print("Vali oige number")
else:
    paaris = 0
    paaritu = 0
    x = 0
    while x != mainnumber:
        x = x + 1
        print(int(x), end= " ")
        if x % 2 == 0: paaris += 1
        else: paaritu += 1
print("Paaris numbrid: " + str(paaris))
print("Paaritu numbrid: " + str(paaritu))