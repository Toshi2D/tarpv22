def readFile(filename):
    file = open(filename, "r")
    lista = []
    for line in file:
        lista.append(line.strip())
    file.close()
    return lista

def writeFile(filename:str, lista:list):
    file = open(filename, "w", encoding="utf-8-sig")
    for line in lista:
        file.write(line+"\n")
    file.close()

def translate(word, eng, rus):
    if word in eng:
        index = eng.index(word)
        print(str(word) + " on russian is " + str(rus[index]))
    elif word in rus:
        index = rus.index(word)
        print(str(word) + " on english is " + str(eng[index]))
    else:
        error = "error"
        return error

def addword(eng, rus):
    engword = input("Type word on english: ")
    rusword = input("Type same word on russian: ")
    eng.append(engword)
    rus.append(rusword)
    print()

def fixword(eng, rus):
    language = int(input("1 - if word is english\n2 - if word is russian"))
    if language == 1:
        listlengh = len(eng)
        print(listlengh)
        word = str("Type word u would like to change: ")
        for x in range(listlengh):
            if word == eng[x]:
                wordchange = input("Type changed word: ")
                eng.pop(x)
                eng.insert(x, wordchange)
                break
    elif language == 2:
        listlengh = len(rus)
        print(listlengh)
        word = str(input("Type word u would like to change: "))
        for z in range == rus[z]:
            if word == rus[z]:
                wordchange = input("Type changed word: ")
                rus.pop(z)
                rus.insert(z, wordchange)
                break
    else: print("error")

def test(eng, rus):
    score = 0
    for x in range(len(eng)):
        print(x)
        print("How to translate word: " + eng[x])
        useransw = input(" > ")
        if useransw == rus[x]:
            print("Correct!")
            score = score + 1
        else: print("Incorrect!")
        print(score)
    result = score / x * 100
    print("Your score is " + str(result))