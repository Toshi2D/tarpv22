import module

russian = []
english = []
russian=module.readFile("dictionary/rus.txt")
english=module.readFile("dictionary/eng.txt")
print(russian)
print(english)
print()

while True:
    userinput = int(input("1 - read file \n2 - add new word \n3 - translate word\n4 - find mistake\n5 - write changes\n6 - test: "))
    print()
    if userinput == 1:
        print(russian)
        print(english)
        print()

    if userinput == 2:
        module.addword(english, russian)

    if userinput == 3:
        word = str(input("Type word: "))
        check = module.translate(word, english, russian)
        if check == "error":
            print("Word not found! Adding new word...")
            module.addword(english, russian)
        else:
            module.translate(word, english, russian)
    if userinput == 4:
        module.fixword(english, russian)
    
    if userinput == 5:
        module.writeFile("dictionary/rus.txt", russian)
        module.writeFile("dictionary/eng.txt", english)
    
    if userinput == 6:
        module.test(english, russian)