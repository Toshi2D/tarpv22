from tkinter import *

loginBase = []
passwordBase = []

window = Tk()
window.geometry("500x300")
window.title("Login")

name_var = StringVar()
pass_var = StringVar()

def readBase(filename):
    file = open(filename, "r")
    lista = []
    for line in file:
        lista.append(line.strip())
    file.close()
    return lista

def writeBase(filename, lista):
    file = open(filename, "w")
    for line in lista:
        file.write(line+"\n")
    file.close()

def verification():
    loginVerify = 0
    passwordVerify = 0
    login = name_var.get()
    password = pass_var.get()

    if login in loginBase:
        loginVerify = 1

    else:
        print(" ! Incorrect login")
        loginVerify = 0
        loginent.configure(bg = "red")

    if password in passwordBase:
        passwordVerify = 1

    else:
        print(" ! Incorrect password")
        passwordVerify = 0
        passwordent.configure(bg = "red")

    if loginVerify == 1 and passwordVerify == 1:
        print(" ! Log in")
        profile()

def registration():
    registrationWindow = Toplevel()
    registrationWindow.title("Registration")
    registrationWindow.geometry("300x200")

    registrationLogin_var = StringVar()
    registrationPass_var = StringVar()
    registrationPassConfirm_var = StringVar()

    def loginPasswordVerification():
        getRegistrationLogin = registrationLogin_var.get()
        getRegistrationPass = registrationPass_var.get()
        def passwordVerification():
            passver = 0
            getRegistrationLogin = registrationLogin_var.get()
            getRegistrationPass = registrationPass_var.get()
            getRegistrationPassConfirm = registrationPassConfirm_var.get()

            print(getRegistrationPass, getRegistrationPassConfirm)
            if getRegistrationPass == getRegistrationPassConfirm:
                print(" ! Password is verified")
                passver = 1

            else:
                print(" ! Incorrect passwords")
                registrationPassEnt.configure(bg = "red")
                registrationPassConfirmEnt.configure(bg = "red")
                passver = 0
            return passver
        def loginVerification():
            logver = 0
            getRegistrationLogin = registrationLogin_var.get()

            if getRegistrationLogin in loginBase:
                print(" ! User name is already taken")
                logver = 0
            else:
                print(" ! User name is avaliable")
                logver = 1
            return logver

        if loginVerification() == 1 and passwordVerification() == 1:
            print(" ! Account is created")
            loginBase.append(getRegistrationLogin)
            writeBase("login.txt", loginBase)
            writeBase("password.txt", passwordBase)

    def tempTextRegistrationLogin(e):
        registrationLoginEnt.delete(0, "end")
        registrationLoginEnt.configure(bg = "SystemButtonFace")

    def tempTextRegistrationPass(e):
        registrationPassEnt.delete(0, "end")
        registrationPassEnt.configure(bg = "SystemButtonFace")

    def tempTextRegisterPassConfirm(e):
        registrationPassConfirmEnt.delete(0, "end")
        registrationPassConfirmEnt.configure(bg = "SystemButtonFace")

    registrationLoginEnt = Entry(registrationWindow, textvariable = registrationLogin_var)
    registrationLoginEnt.insert(0, "Login")
    registrationPassEnt = Entry(registrationWindow, textvariable = registrationPass_var)
    registrationPassEnt.insert(0, "Password")
    registrationPassConfirmEnt = Entry(registrationWindow, textvariable = registrationPassConfirm_var)
    registrationPassConfirmEnt.insert(0, "Confirm Password")
    submitbtnreg = Button(registrationWindow, text = "Submit", command = loginPasswordVerification)

    registrationLoginEnt.bind("<FocusIn>", tempTextRegistrationLogin)
    registrationPassEnt.bind("<FocusIn>", tempTextRegistrationPass)
    registrationPassConfirmEnt.bind("<FocusIn>", tempTextRegisterPassConfirm)

    registrationLoginEnt.pack(side = TOP)
    registrationPassEnt.pack(side = TOP)
    registrationPassConfirmEnt.pack(side = TOP)
    submitbtnreg.pack(side = TOP)

def profile():
    profileWindow = Toplevel()
    getUserName = name_var.get()
    profileWindow.title(getUserName)
    profileWindow.geometry("500x400")

    def destroyProfileWindow():
        profileWindow.destroy()
        print(" ! Log out")
    
    logoutbtn = Button(profileWindow, text="Log out", command=destroyProfileWindow)

    logoutbtn.pack(side = TOP)

def changepassword():
    changepasswordWindow = Toplevel()
    changepasswordWindow.title("Change Password")
    changepasswordWindow.geometry("300x200")

def showpassword():
    showpasswordWindow = Toplevel()
    showpasswordWindow.title("Show Password")
    showpasswordWindow.geometry("300x200")

    userLogin = StringVar()

    def showPassVerif():
        userLogin = userLogin.get()
        print(userLogin)
        if userLogin in loginBase:
            print(passwordBase)
        else:
            print(" ! Incorrect Login")

    userLoginInput = Entry(showpasswordWindow, textvariable = userLogin)
    userLogin = userLogin.get()
    print(userLogin)

    submitbtn = Button(showpasswordWindow, text="Submit", command=showPassVerif)
    userLoginInput.pack(side = TOP)
    submitbtn.pack(side = TOP)

def tempTextLogin(e):
    loginent.delete(0, "end")
    loginent.configure(bg = "SystemButtonFace")

def tempTextPass(e):
    passwordent.delete(0, "end")
    passwordent.configure(bg = "SystemButtonFace")

loginBase = readBase("login.txt")
passwordBase = readBase("password.txt")

loginent = Entry(window,textvariable = name_var)
loginent.insert(0, "Login")
passwordent = Entry(window, textvariable = pass_var, show="*")
passwordent.insert(0, "Password")
submitbtn = Button(window, text="Submit", command=verification)
registrationbtn = Button(window, text="Registration", command=registration)
changepasswordbtn = Button(window, text="Change Password", command=changepassword)
showpasswordbtn = Button(window, text="Show Password", command=showpassword)

loginent.bind("<FocusIn>", tempTextLogin)
passwordent.bind("<FocusIn>", tempTextPass)

loginent.pack(side = TOP)
passwordent.pack(side = TOP)
submitbtn.pack(side = TOP)
registrationbtn.pack(side = TOP)
changepasswordbtn.pack(side = TOP)
showpasswordbtn.pack(side = TOP)

print(name_var)
window.mainloop()
