from msilib.schema import RadioButton
from tkinter import *
k=0

def klikker(event):
    global k
    k+=1
    btn.configure(text=k)

def klikkermaha(event):
    global k
    k-=1
    btn.configure(text=k)

def tekst_to_lbl(event):
    t=ent.get()
    lbl.configure(text=t)
    ent.delete(0,END) #2,7

def valik():
    val=var.get()
    ent.insert(END,val)

def checklist_to_l(event):
    v1=var1.get()
    v2=var2.get()
    jarjend=[v1, v2]
    l.delete(0, 1)
    for item in jarjend:
        l.insert(END, item)

aken = Tk()
#aken.geometry("400x500")
aken.title("Minu esimene aken")
aken.iconbitmap("icon.ico")
f = Frame(aken, bg="black")

lbl = Label(f, text="Elemendid", bg="gold", fg="#AA4A44", font="Arial 20", height=5, width=15)
btn = Button(f, text="Vajuta siia", font="Arial 24", relief=GROOVE, width=15)#SUNKEN, RAISED
ent = Entry(f, fg="blue", bg="lightblue", width=15, font="Arial 20", justify=CENTER)

var = IntVar()
r1 = Radiobutton(f, text="esimene", font="Arial 20", variable=var, value=1, command=valik)
r2 = Radiobutton(f, text="teine", font="Arial 20", variable=var, value=2, command=valik)
r3 = Radiobutton(f, text="kolmas", font="Arial 20", variable=var, value=3, command=valik)

var1 = StringVar()
var2 = StringVar()
c1 = Checkbutton(f, text="esimene", font="Arial 20", variable=var1, onvalue="esimene on valitud", offvalue="esimene on peidatud")
c2 = Checkbutton(f, text="teine", font="Arial 20", variable=var2, onvalue="teine on valitud", offvalue="teine on peidatud")

l = Listbox(f, height=2, width=20)

tahvel = Canvas(aken, width=260, height=260, bg="black")
img=PhotoImage(file="Tree-256x256.png")
tahvel.create_image(2, 2, image=img, anchor=NW)

btn.bind("<Button-1>",klikker) #lkm
btn.bind("<Button-2>", checklist_to_l)
btn.bind("<Button-3>",klikkermaha) #pkm
ent.bind("<Return>",tekst_to_lbl)#Enter
r1.pack()
r2.pack()
r3.pack()
l.pack()
c1.deselect()
c2.deselect()
c1.pack()
c2.pack()
lbl.pack()
btn.pack(side=LEFT)
ent.pack(side=LEFT)
f.grid(row=0, column=0)
tahvel.grid(row=0, column=1)
aken.mainloop()
